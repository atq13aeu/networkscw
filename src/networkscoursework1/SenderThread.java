/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package networkscoursework1;

import CMPC3M06.AudioRecorder;
import java.io.IOException;
import java.lang.System;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.DatagramSocket2;
import uk.ac.uea.cmp.voip.DatagramSocket3;
import uk.ac.uea.cmp.voip.DatagramSocket4;

/**
 *
 * @author kff12ynu
 */
public class SenderThread implements Runnable {

    static DatagramSocket sending_socket = null;

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }
    int socket = 0;

    private int[] getIndexes(int d) {
        int[] a = new int[d * d];
        int ctr = 0;
        for (int i = 0; i < d; i++) {
            for (int j = 0; j < d; j++) {
                a[ctr] = j * d + (d - 1 - i);
                ctr++;
            }
        }
        return a;
    }

    @Override
    public void run() {
        //Port to send to
        int PORT = 55555;

        //DatagramPacket[] matrix = new DatagramPacket[16];
        //DatagramPacket[] matrix2 = new DatagramPacket[16];
        ArrayList<byte[]> matrix = new ArrayList<>();
        ArrayList<byte[]> matrix2 = new ArrayList<>();

        int[] interleaverIndexes = this.getIndexes(4);

        for (int i = 0; i < 16; i++) {
            matrix.add(new byte[0]);
            matrix2.add(new byte[0]);
        }

        //IP ADDRESS to send to
        InetAddress clientIP = null;
        try {
            clientIP = InetAddress.getByName("localhost");  //CHANGE localhost to IP or NAME of client machine
        } catch (UnknownHostException e) {
            System.out.println("ERROR: Sender: Could not find client IP");
            e.printStackTrace();
            System.exit(0);
        }

        //Open a socket to send from
        //We dont need to know its port number as we never send anything to it.
        //We need the try and catch block to make sure no errors occur.
        try {
            switch (socket) {
                case 1:
                default:
                    sending_socket = new DatagramSocket();
                    System.out.println("DatagramSocket selected");
                    break;
                case 2:
                    sending_socket = new DatagramSocket2();
                    System.out.println("DatagramSocket2 selected");
                    break;
                case 3:
                    sending_socket = new DatagramSocket3();
                    System.out.println("DatagramSocket3 selected");
                    break;
                case 4:
                    sending_socket = new DatagramSocket4();
                    System.out.println("DatagramSocket4 selected");
                    break;
            }

        } catch (SocketException e) {
            System.out.println("ERROR: Sender: Could not open UDP socket to send from.");
            e.printStackTrace();
            System.exit(0);
        }

        AudioRecorder recorder = null;
        try {
            //Initialise AudioPlayer and AudioRecorder objects
            recorder = new AudioRecorder();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(SenderThread.class.getName()).log(Level.SEVERE, null, ex);
        }

        boolean running = true;

        System.out.println("Sender Running");
        int ctr = 0;
        int sentctr = 0;
        int dubBuf = 0;
        boolean initialDelay = true;

        while (running) {
            byte[] block;
            byte[] packetData = new byte[515];
            try {
                block = recorder.getBlock();

                packetData[0] = (byte) 0xDE;
                packetData[1] = (byte) ctr;
                packetData[2] = (byte) dubBuf;

                System.arraycopy(block, 0, packetData, 3, block.length);
                if (dubBuf == 0) {
                    matrix.set(ctr, packetData);
                } else  {
                    matrix2.set(ctr, packetData);
                }
// System.out.println(dubBuf + " ----------");
                if (initialDelay == false) {

                  //  System.out.printf("%d, %d\n", sentctr, interleaverIndexes[sentctr]);
                    if (dubBuf == 1) {
                        packetData = matrix.get(interleaverIndexes[sentctr]);
                    } else {
                        packetData = matrix2.get(interleaverIndexes[sentctr]);
                    }
                    //Make a DatagramPacket from it, with client address and port number
                    DatagramPacket packet = new DatagramPacket(packetData, packetData.length, clientIP, PORT);
                    sending_socket.send(packet);                    
                    sentctr++;

                    if (sentctr >= 16) {
                        sentctr = 0;
                    }
                } else {
                    if (ctr >= 15) {
                        initialDelay = false;
                    }
                }

            } catch (IOException e) {
                System.out.println("ERROR: Sender: Some random IO error occured!");
                e.printStackTrace();
            }
            ctr++;

            //calculate delay ((d*d) - d)
            if (ctr >= 16) {
                ctr = 0;
                if (dubBuf == 0) {
                    dubBuf++;
                } else {
                    dubBuf--;
                }
            }
        }
        //Close the socket
        sending_socket.close();
        recorder.close();
    }

}
