/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package networkscoursework1;

import CMPC3M06.AudioPlayer;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.DatagramSocket2;
import uk.ac.uea.cmp.voip.DatagramSocket3;
import uk.ac.uea.cmp.voip.DatagramSocket4;

/**
 *
 * @author kff12ynu
 */
/**
 * Datagram packet loss stuff. 1 - normal, no packet loss. 2 - misses some,
 * could be in pattern. 3 - misses out some, and reorders.
 *
 * 4 - dodgy packets, some empty some corrupting.
 *
 * @author atq13aeu
 */
public class ReceiverThread implements Runnable {

    static DatagramSocket receiving_socket;

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {

        //Port to open socket on
        int PORT = 55555;

        //Open a socket to receive from on port PORT
        //DatagramSocket receiving_socket;
        InetAddress serverIP = null;
        try {
            serverIP = InetAddress.getByName("localhost");  //CHANGE localhost to IP or NAME of client machine
        } catch (UnknownHostException e) {
            System.out.println("ERROR: Sender: Could not find client IP");
            e.printStackTrace();
            System.exit(0);
        }

        try {
            receiving_socket = new DatagramSocket(PORT, serverIP);
        } catch (SocketException e) {
            System.out.println("ERROR: TextReceiver: Could not open UDP socket to receive from.");
            e.printStackTrace();
            System.exit(0);
        }

        AudioPlayer player = null;
        try {
            player = new AudioPlayer();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(ReceiverThread.class.getName()).log(Level.SEVERE, null, ex);
        }

        boolean running = true;
        System.out.println("Receiver Running");

        int start_ctr = 0;
        boolean started_playing = false;
        int i = 0;
        DatagramPacket[] packetStore = new DatagramPacket[16];
        byte[][] byteStore = new byte[16][512];
        byte[][] byteStore2 = new byte[16][512];
        for (int j = 0; j < byteStore.length; j++) {
            byteStore[j] = null;
            byteStore2[j] = null;
        }
        int plyrctr = 0;
        byte[] lastBuffer = new byte[515];
        int dubBuffer;
        while (running) {

            try {
                //Receive a DatagramPacket 
                byte[] buffer = new byte[515];
                DatagramPacket packet = new DatagramPacket(buffer, 515);

                receiving_socket.receive(packet);

                System.out.println(buffer[1]);

                if (buffer[0] != (byte) 0xDE) {
                    // got a nefarious packet
                    System.out.println("Got a dodgy packet");
                    //fill that element with previous for now. 
                    buffer[2] = lastBuffer[0];

                    continue;
                }

                byte[] headerinfo = new byte[3];
                System.arraycopy(buffer, 0, headerinfo, 0, 3);

                dubBuffer = headerinfo[2];
                //System.out.println("[][][}:" + dubBuffer);

                byte[] blocke = new byte[512];
                System.arraycopy(buffer, 3, blocke, 0, 512);

                // packet = new DatagramPacket(blocke, 512);
                if (dubBuffer == 1) {
                    byteStore[(int) headerinfo[1]] = blocke;
                    System.out.println((int) headerinfo[1] + "dubBuffer == 1");
                } else if (dubBuffer == 0) {

                    byteStore2[(int) headerinfo[1]] = blocke;
                    System.out.println((int) headerinfo[1] + "dubBuffer == 0");
                }

                if (!started_playing) {

                    start_ctr++;
                    if (start_ctr > 15) {
                        started_playing = true;
                    }
                }

                byte[] emptyBlock = new byte[512];
                if (started_playing) {
                    byte[] block = new byte[512];
                    //System.out.println((int) block[0]);
                    if (dubBuffer == 0) {
                        block = byteStore[plyrctr];
                    } else {
                        block = byteStore2[plyrctr];
                    }

                    //System.arraycopy(byteStoreLocal, 0, block, 0, 512);
                    if(block !=null){
                    player.playBlock(block);
                    }

                    //this is wrong
                    //also add a checksum.
                    //job done
                    
                    if (dubBuffer == 1) {
                        if (plyrctr >= 15) {
                            
                            if (byteStore[0] == null) {
                                System.out.println("empty, just a heads up and at 15");
                                byteStore[0] = block;
                            }
                        } else {
                            if (byteStore[plyrctr + 1] == null) {
                                byteStore[plyrctr + 1] = block;
                                //Splicing if can be bothered
                                System.out.println("empty, just a heads up");
                            }
                            
                        }
                        byteStore2[plyrctr] = null;
                        
                    
                    } else {
                        if (plyrctr >= 15) {

                            if (byteStore2[0] == null) {
                                System.out.println("empty, just a heads up and at 15");
                                byteStore2[0] = block;
                            }
                        } else {
                            if (byteStore2[plyrctr + 1] == null) {
                                byteStore2[plyrctr + 1] = block;
                                //Splicing if can be bothered
                                System.out.println("empty, just a heads up___________0");
                            }
                        }
                        byteStore2[plyrctr] = null;
                    }

                    

                    plyrctr++;
                    if (plyrctr >= 16) {
                        plyrctr = 0;
                    }

                    lastBuffer[0] = buffer[2];
                }

            } catch (IOException e) {
                System.out.println("ERROR: TextReceiver: Some random IO error occured!");
                e.printStackTrace();
            }

        }

        //Close the socket
        receiving_socket.close();

        player.close();
    }

    /* public ArrayList<DatagramPacket> reorder(ArrayList<DatagramPacket> outOfOrder ) {

     ArrayList<DatagramPacket> inOrder = new ArrayList<DatagramPacket>();
     // B = [int32, int32, int32, int32, int32]
     System.out.println(outOfOrder.size() + "____size________test");
     int minValue = 255;
     for (int i = 0; i < outOfOrder.size(); i++) {
     byte[] currentPacket = outOfOrder.get(i).getData();
     if (currentPacket[1] < minValue)
     minValue = (int) currentPacket[0];
     }
        
     System.out.println(minValue);
        
     for (int i = 0; i < outOfOrder.size(); i++) {
     System.out.println(outOfOrder.size() + "____size________test");
     byte[] currentPacket = outOfOrder.get(i).getData();
     //byte[] C = new byte[521];
     //C = [byte, byte, ..., byte, byte]
     //System.arraycopy(A.get(i).getData(), 1, C[i], 0, 8);
     inOrder.set((int) (currentPacket[1] - minValue), outOfOrder.get(i));
     //B[i] = C[i];    
     // System.out.println(B[i] + "____________test");
     }
                
               
        
     /*for (int i = 0; i < A.size(); i++) {
     for (int j = 0; j < i; j++) {
     //System.arraycopy(A.get(j), 0, A.get(j), 0, 8);
                

                

                

     }
     }

     return inOrder;
     }*/
}
