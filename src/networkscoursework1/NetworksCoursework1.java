/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package networkscoursework1;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 *
 * @author kff12ynu
 */
public class NetworksCoursework1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Enter Datagram socket to be used");
        int socket = 0;
        Scanner scan = new Scanner(System.in);
        socket = scan.nextInt();

        ReceiverThread receiver = new ReceiverThread();
        SenderThread sender = new SenderThread();
        sender.socket = socket;

        receiver.start();
        sender.start();

    }

}
